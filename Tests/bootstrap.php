<?php
/**
 * @package: Curl
 *
 * @author: daniel
 * @date: 18.08.13
 * @time: 09:22
 */

use Composer\Autoload\ClassLoader;

$file = __DIR__ . '/../vendor/autoload.php';

if (!file_exists($file)) {
    throw new \RuntimeException('Install dependencies to run test suite. "php composer.phar install --dev"');
}

/** @var ClassLoader $loader */
$loader = require $file;
