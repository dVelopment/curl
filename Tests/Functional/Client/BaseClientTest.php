<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 15:06
 */

namespace DVelopment\Tests\Functional\Client;


use DVelopment\Curl\Client;
use DVelopment\Tests\Functional\BaseTest;
use JMS\Serializer\Serializer;

abstract class BaseClientTest extends BaseTest
{
    /**
     * @param Serializer $serializer
     * @return Client
     */
    protected function createClient(Serializer $serializer = null)
    {
        return new Client($serializer);
    }
}