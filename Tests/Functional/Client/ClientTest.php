<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 15:03
 */

namespace DVelopment\Tests\Functional\Client;

use DVelopment\Curl\Client;
use DVelopment\Curl\Http\GetRequest;

class ClientTest extends BaseClientTest
{
    public function testClient()
    {
        $client = $this->createClient();
        $request = $this->createEmptyRequest();

        $response = $client->execute($request);

        $this->assertEquals(200, $response->getStatus());
    }

    public function testJson()
    {
        $client = $this->createClient();
        $request = $this->createEmptyRequest('https://my.fastbill.com/api/1.0/api.php', array(), 'json');
        $request->setContentType('application/json');

        $response = $client->execute($request);
        print_r($response->getContent());
    }
}