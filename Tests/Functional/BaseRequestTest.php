<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 14:33
 */

namespace DVelopment\Tests\Functional;


use DVelopment\Curl\Http\GetRequest;
use DVelopment\Curl\Http\PostRequest;

abstract class BaseTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @param string $url
     * @param array $params
     * @param string $format
     * @param bool $followRedirects
     * @param bool $binary
     * @param bool $useAuthentication
     * @return GetRequest
     */
    protected function createEmptyRequest($url = 'www.google.com', array $params = array(), $format = 'raw', $followRedirects = true, $binary = false, $useAuthentication = false)
    {
        return new GetRequest($url, $params, $format, $followRedirects, $binary, $useAuthentication);
    }

    /**
     * @param string $url
     * @param array $params
     * @param string $format
     * @param bool $followRedirects
     * @param bool $binary
     * @param bool $useAuthentication
     * @return GetRequest
     */
    protected function createEmptyPostRequest($url = 'www.google.com', array $params = array(), $format = 'raw', $followRedirects = true, $binary = false, $useAuthentication = false)
    {
        return new PostRequest($url, $params, $format, $followRedirects, $binary, $useAuthentication);
    }
}