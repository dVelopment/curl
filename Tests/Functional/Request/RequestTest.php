<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 12:25
 */

namespace DVelopment\Tests\Functional\Request;

use DVelopment\Curl\Http\GetRequest;
use DVelopment\Tests\Functional\BaseTest;

class RequestTest extends BaseTest
{
    public function testInvalidUrl()
    {
        try {
            $request = $this->createEmptyRequest('foobar:foo');
            $this->fail('InvalidArgumentException expected');
        } catch (\InvalidArgumentException $e) {
            // nothing to do
        }

    }

    public function testPort()
    {
        try {
            $request = $this->createEmptyRequest('localhost:9200');
        } catch (\InvalidArgumentException $e) {
            $this->fail('Ports should be supported');
        }
    }

    public function testInvalidScheme()
    {
        try {
            $request = new GetRequest('ftp://localhost');
            $this->fail('InvalidArgumentException expected');
        } catch(\InvalidArgumentException $e) {
            $this->assertContains('scheme', $e->getMessage());
        }
    }

    public function testMethod()
    {
        $request = $this->createEmptyRequest();
        $request->setMethod('post');

        $this->assertEquals('POST', $request->getMethod());
    }

    public function testFormat()
    {
        $request = $this->createEmptyRequest();
        $this->assertEquals('raw', $request->getExpectedFormat());

        $request->setFormat('json');
        $this->assertEquals('json', $request->getExpectedFormat());
    }

    public function testBinary()
    {
        $request = $this->createEmptyRequest();

        $this->assertFalse($request->isBinary());

        $request = $this->createEmptyRequest('google.com', array(), 'raw', true, true);

        $this->assertTrue($request->isBinary());

        $request->setBinary(false);
        $this->assertFalse($request->isBinary());
    }

    public function testFollowRedirects()
    {
        $request = $this->createEmptyRequest();
        $this->assertTrue($request->followRedirects());

        $request = $this->createEmptyRequest('www.google.com', array(), 'raw', false);
        $this->assertFalse($request->followRedirects());

        $request->setFollowRedirects(true);
        $this->assertTrue($request->followRedirects());
    }

    public function testAuthentication()
    {
        $request = $this->createEmptyRequest();
        $this->assertFalse($request->useAuthentication());
        $this->assertEmpty($request->getUsername());
        $this->assertEmpty($request->getPassword());

        $request = $this->createEmptyRequest('google.com', array(), 'raw', true, false, true);
        $this->assertTrue($request->useAuthentication());

        $request->setUsername('user')
            ->setPassword('pass');
        $this->assertEquals('user', $request->getUsername());
        $this->assertEquals('pass', $request->getPassword());

        $request->setUseAuthentication(false);
        $this->assertFalse($request->useAuthentication());
    }
}