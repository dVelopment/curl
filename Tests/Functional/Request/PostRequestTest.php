<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 14:32
 */

namespace DVelopment\Tests\Functional\Request;


use DVelopment\Curl\Exception\FileNotFoundException;
use DVelopment\Tests\Functional\BaseTest;

class PostRequestTest extends BaseTest
{
    public function testBody()
    {
        $request = $this->createEmptyPostRequest();
        $this->assertEmpty($request->getParams());

        $request = $this->createEmptyPostRequest('google.com', array('foo' => 'bar'));
        $this->assertEquals('foo=bar', $request->getParams());
    }

    public function testInvalidFile()
    {
        $file = 'somefile.txt';
        try {
            $request = $this->createEmptyPostRequest('google.com', array('foo' => 'bar', 'file' => '@' . $file));
            $this->fail('FileNotFoundException expected');
        } catch (FileNotFoundException $e) {
            $this->assertContains($file, $e->getMessage());
        }
    }

    public function testInvalidFileAdd()
    {
        $file = 'somefile.txt';
        $request = $this->createEmptyPostRequest();

        try {
            $request->addFile('file', $file);
            $this->fail('FileNotFoundException expected');
        } catch (FileNotFoundException $e) {
            $this->assertContains($file, $e->getMessage());
        }
    }

    public function testValidFile()
    {
        try {
            $request = $this->createEmptyPostRequest('google.com', array('file' => '@' . __FILE__));
        } catch (FileNotFoundException $e) {
            $this->fail('this exception shouldn\'t have been thrown');
        }
    }

    public function testValidFileAdd()
    {
        $request = $this->createEmptyPostRequest();
        try {
            $request->addFile('file', __FILE__);
        } catch (FileNotFoundException $e) {
            $this->fail('this exception shouldn\'t have been thrown');
        }
    }
}