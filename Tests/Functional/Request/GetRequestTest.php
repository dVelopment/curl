<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 12:21
 */

namespace DVelopment\Tests\Functional\Request;

use DVelopment\Curl\Http\GetRequest;
use DVelopment\Tests\Functional\BaseTest;

class GetRequestTest extends BaseTest
{
    /**
     * test a request with empty parameters
     */
    public function testEmpty()
    {
        $request = new GetRequest('http://www.google.com');

        $this->assertEquals('http://www.google.com', $request->getRequestUrl());
        $this->assertEmpty($request->getParams());
    }

    public function testWithoutScheme()
    {
        $foo = array();
        $this->assertEmpty($foo);
        $request = new GetRequest('www.google.com');

        $this->assertEquals('http://www.google.com', $request->getRequestUrl());
    }

    public function testWithParams()
    {
        $request = new GetRequest('http://www.google.com', array('foo' => 'bar', 'foo2' => 'barfoo'));

        $this->assertEquals('http://www.google.com?foo=bar&foo2=barfoo', $request->getRequestUrl());
        $this->assertEmpty($request->getParams());
    }
}