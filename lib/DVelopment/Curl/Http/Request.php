<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 09:58
 */

namespace DVelopment\Curl\Http;


use DVelopment\Curl\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\ParameterBag;

abstract class Request implements RequestInterface
{
    /**
     * @var ParameterBag
     */
    private $params;

    /**
     * @var string
     */
    private $method = 'GET';

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $format = 'raw';

    /**
     * @var boolean
     */
    private $binary = false;

    /**
     * @var boolean
     */
    private $followRedirects;

    /**
     * use HTTP base authentication
     *
     * @var boolean
     */
    private $useAuthentication = false;

    /**
     * username for HTTP base authentication
     *
     * @var string
     */
    private $username = '';

    /**
     * password for HTTP base authentication
     *
     * @var string
     */
    private $password = '';

    /**
     * @var boolean
     */
    private $hasFile = false;

    /**
     * @var ParameterBag
     */
    private $headers;

    /**
     * @var string
     */
    private $contentType = 'text/plain';

    /**
     * @var string
     */
    private $body = '';

    /**
     * @param string $url
     * @param string $method
     * @param array $params
     * @param string $format
     * @param bool $followRedirects
     * @param bool $binary
     * @param bool $useAuthentication
     * @throws \InvalidArgumentException
     */
    public function __construct($url, $method = 'GET', array $params = array(), $format = 'raw', $followRedirects = true, $binary = false, $useAuthentication = false)
    {
        $scheme = parse_url($url, PHP_URL_SCHEME);
        if (!$scheme && 0 !== strpos($url, 'http')) {
            $prefix = 'http';
            $scheme = 'http';
            if (':' !== substr($url, 0, 1)) {
                $prefix .= ':';
                if ('//' !== substr($url, 0, 2)) {
                    $prefix .= '//';
                }
            }
            $url = $prefix . $url;
        }
        if (false === filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \InvalidArgumentException('invalid URL given: ' . $url);
        }

        if (0 !== strpos($scheme, 'http')) {
            throw new \InvalidArgumentException('unsupported scheme: ' . $scheme);
        }


        $this->url = $url;
        $this->setMethod($method);
        $this->params = new ParameterBag();
        $this->setFormat($format);
        $this->setBinary($binary);
        $this->setUseAuthentication($useAuthentication);
        $this->setFollowRedirects($followRedirects);
        $this->headers = new ParameterBag();

        foreach ($params as $key=>$value) {
            $this->addParameter($key, $value);
        }
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return Request
     */
    public function setMethod($method)
    {
        $this->method = strtoupper($method);

        return $this;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     *
     * @return Request
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getBinary()
    {
        return $this->binary;
    }

    /**
     * @param boolean $binary
     *
     * @return Request
     */
    public function setBinary($binary)
    {
        $this->binary = $binary;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getFollowRedirects()
    {
        return $this->followRedirects;
    }

    /**
     * @param boolean $followRedirects
     *
     * @return Request
     */
    public function setFollowRedirects($followRedirects)
    {
        $this->followRedirects = $followRedirects;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getUseAuthentication()
    {
        return $this->useAuthentication;
    }

    /**
     * @param boolean $useAuthentication
     *
     * @return Request
     */
    public function setUseAuthentication($useAuthentication)
    {
        $this->useAuthentication = $useAuthentication;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return Request
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return Request
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * get the request body
     *
     * @return mixed
     */
    public function getParams()
    {
        if ('GET' === $this->method) {
            return null;
        }

        return http_build_query($this->params->all());
    }

    /**
     * the expected response format
     * possible values:
     * - raw: returns the response without modification
     * - json: will interpret the response as a JSON object graph
     * - xml: will interpret the response as a XML document
     * - yaml: will interpret the response as a Yaml document
     *
     * @return string
     */
    public function getExpectedFormat()
    {
        return $this->getFormat();
    }

    /**
     * the URL to call
     *
     * @return string
     */
    public function getRequestUrl()
    {
        if ('GET' !== $this->getMethod() || 0 === $this->params->count()) {
            return $this->url;
        }

        if (defined('PHP_QUERY_RFC3986')) {
            $query = http_build_query($this->params->all(), null, null, PHP_QUERY_RFC3986);
        } else {
            $query = str_replace('+', '%20', http_build_query($this->params->all()));
        }

        return $this->url . '?' . $query;
    }

    /**
     * return the raw bytes
     *
     * @return boolean
     */
    public function isBinary()
    {
        return $this->getBinary();
    }

    /**
     * follow redirects?
     *
     * @return boolean
     */
    public function followRedirects()
    {
        return $this->getFollowRedirects();
    }

    /**
     * use HTTP authentication?
     *
     * @return boolean
     */
    public function useAuthentication()
    {
        return $this->getUseAuthentication();
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addParameter($key, $value)
    {
        if (0 === strpos($value, '@')) {
            $this->addFile($key, substr($value, 1));
        } else {
            $this->params->set($key, $value);
        }
    }

    /**
     * @param string $key
     * @param string $fileName
     */
    public function addFile($key, $fileName)
    {
        if (!file_exists($fileName)) {
            throw new FileNotFoundException($fileName);
        }
        $this->hasFile = true;
        $this->params->set($key, '@' . $fileName);
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return array_merge($this->headers->all(), array('Content-Type: ' . $this->contentType));
    }

    /**
     * @param string $contentType
     *
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     *
     * @return Request
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }
}