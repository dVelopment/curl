<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 10:47
 */

namespace DVelopment\Curl\Http;


use Symfony\Component\HttpFoundation\ParameterBag;

class Response implements ResponseInterface
{
    /**
     * @var ParameterBag $headers
     */
    private $headers;

    /**
     * @var string
     */
    private $rawContent = '';

    /**
     * @var object
     */
    private $formattedContent = null;

    /**
     * @var int
     */
    private $statusCode = 200;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @param RequestInterface $request
     * @param string $rawContent
     * @param int $statusCode
     */
    public function __construct(RequestInterface $request, $rawContent = '', $statusCode = 200)
    {
        $this->request = $request;
        $this->setRawContent($rawContent);
        $this->setStatusCode($statusCode);
        $this->headers = new ParameterBag();
    }

    /**
     * @return string
     */
    public function getRawContent()
    {
        return $this->rawContent;
    }

    /**
     * @param string $rawContent
     *
     * @return Response
     */
    public function setRawContent($rawContent)
    {
        $this->rawContent = $rawContent;

        return $this;
    }

    /**
     * @return object
     */
    public function getFormattedContent()
    {
        return $this->formattedContent;
    }

    /**
     * @param object $formattedContent
     *
     * @return Response
     */
    public function setFormattedContent($formattedContent)
    {
        $this->formattedContent = $formattedContent;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     *
     * @return Response
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->getFormattedContent();
    }

    /**
     * @return RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return ParameterBag
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param string $headers
     */
    public function parseHeaders($headers)
    {
        foreach (explode("\n", $headers) as $header) {
            if ($idx = strpos($header, ':')) {
                $key = substr($header, 0, $idx);

                if ($value = trim(substr($header, $idx + 1))) {
                    $this->addHeader($key, $value);
                }
            }
        }
    }

    /**
     * @param string $key
     * @param string $value
     */
    private function addHeader($key, $value)
    {
        $this->headers->set($key, $value);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->getStatusCode();
    }
}