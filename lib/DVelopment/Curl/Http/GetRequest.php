<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 13:57
 */

namespace DVelopment\Curl\Http;


use Symfony\Component\HttpFoundation\ParameterBag;

class GetRequest extends Request
{
    public function __construct($url, array $params = array(), $format = 'raw', $followRedirects = true, $binary = false, $useAuthentication = false)
    {
        parent::__construct($url, 'GET', $params, $format, $followRedirects, $binary, $useAuthentication);
    }
}