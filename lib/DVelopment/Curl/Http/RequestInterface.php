<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 09:58
 */

namespace DVelopment\Curl\Http;


interface RequestInterface
{
    /**
     * the URL to call
     *
     * @return string
     */
    public function getRequestUrl();

    /**
     * the HTTP method to use
     *
     * @return string
     */
    public function getMethod();

    /**
     * follow redirects?
     *
     * @return boolean
     */
    public function followRedirects();

    /**
     * use HTTP authentication?
     *
     * @return boolean
     */
    public function useAuthentication();

    /**
     * the username used for authentication
     *
     * @return string
     */
    public function getUsername();

    /**
     * the password used for authentication
     *
     * @return string
     */
    public function getPassword();

    /**
     * the expected response format
     * possible values:
     * - raw: returns the response without modification
     * - json: will interpret the response as a JSON object graph
     * - xml: will interpret the response as a XML document
     * - yaml: will interpret the response as a Yaml document
     *
     * @return string
     */
    public function getExpectedFormat();

    /**
     * get the request params
     *
     * @return mixed
     */
    public function getParams();

    /**
     * get the request body
     *
     * @return string
     */
    public function getBody();

    /**
     * return the raw bytes
     *
     * @return boolean
     */
    public function isBinary();

    /**
     * @return array
     */
    public function getHeaders();
}