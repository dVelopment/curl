<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 10:47
 */

namespace DVelopment\Curl\Http;


use Symfony\Component\HttpFoundation\ParameterBag;

interface ResponseInterface
{
    /**
     * @return ParameterBag
     */
    public function getHeaders();

    /**
     * @return mixed
     */
    public function getContent();

    /**
     * @return string
     */
    public function getRawContent();

    /**
     * @return RequestInterface
     */
    public function getRequest();

    /**
     * @return int
     */
    public function getStatus();
}