<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 09:55
 */

namespace DVelopment\Curl;


use DVelopment\Curl\Http\RequestInterface;
use DVelopment\Curl\Http\Response;
use DVelopment\Curl\Http\ResponseInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class Client
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer = null)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param RequestInterface $request
     * @param \Symfony\Component\Serializer\Encoder\DecoderInterface $decoder
     * @param string $type
     * @return ResponseInterface
     */
    public function execute(RequestInterface $request, DecoderInterface $decoder = null, $type = '')
    {
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_BINARYTRANSFER  =>  $request->isBinary(),
            CURLOPT_FOLLOWLOCATION  =>  $request->followRedirects(),
            CURLOPT_HEADER          =>  true,
            CURLOPT_HTTPHEADER      =>  $request->getHeaders(),
        );

        switch ($request->getMethod()) {
            case 'GET':
                if ($body = $request->getBody()) {
                    $options[CURLOPT_POSTFIELDS] = $body;
                }
                break;
            case 'POST':
                $options[CURLOPT_POST] = true;
                $options[CURLOPT_POSTFIELDS] = $request->getBody();
                break;
            case 'PUT':
                $options[CURLOPT_PUT] = true;
                $options[CURLOPT_INFILE] = $request->getParams();
                $options[CURLOPT_INFILESIZE] = strlen($request->getParams());
                break;
            default:
                $options[CURLOPT_CUSTOMREQUEST] = true;
                $options[CURLOPT_POSTFIELDS] = $request->getParams();
                break;
        }

        if (true === $request->useAuthentication()) {
            $options[CURLOPT_USERPWD] = $request->getUsername() . ':' . $request->getPassword();
        }

        $ch = curl_init($request->getRequestUrl());
        curl_setopt_array($ch, $options);

        $responseString = curl_exec($ch);
        $headerSize = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $header = substr($responseString, 0, $headerSize);
        $rawResponse = substr($responseString, $headerSize);

        $response = new Response($request, $rawResponse, $status);
        $response->parseHeaders($header);

        if ('raw' === $request->getExpectedFormat()) {
            $response->setFormattedContent($rawResponse);
        } else {
            $format = $request->getExpectedFormat();
            if ($type) {
                $response->setFormattedContent($this->getSerializer($format)->deserialize($rawResponse, $type, $format));
            } else {
                if (!$decoder) {
                    $decoder = $this->getDecoder($format);
                }
                $response->setFormattedContent($decoder->decode($rawResponse, $format));
            }
        }

        return $response;
    }

    /**
     * @param string $format
     * @return Serializer|SerializerInterface
     */
    private function getSerializer($format)
    {
        if (null === $this->serializer) {
            $this->serializer = new Serializer(array(new GetSetMethodNormalizer()), array($this->getDecoder($format)));
        }

        return $this->serializer;
    }

    /**
     * @param string $format
     * @return DecoderInterface
     * @throws \RuntimeException
     */
    private function getDecoder($format)
    {
        if ('json' === $format) {
            return new JsonEncoder();
        } elseif ('xml' === $format) {
            return new XmlEncoder();
        } else {
            throw new \RuntimeException('could not autoload decoder for format: ' . $format);
        }
    }
}