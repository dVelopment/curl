<?php
/**
 * @package Curl
 *
 * @author Daniel Holzmann <d@velopment.at>
 * @date 18.08.13
 * @time 14:48
 */

namespace DVelopment\Curl\Exception;


class FileNotFoundException extends \InvalidArgumentException
{
    public function __construct($file)
    {
        parent::__construct('file not found: ' . $file);
    }
}